export const formatDate = (dateString) => {
  const date = new Date(dateString);
  return date.toLocaleDateString("en-US", {
    month: "2-digit",
    day: "2-digit",
    year: "numeric",
  });
};

export const formatMonetaryValue = (value, isDiff = false) => {
  const formattedValue = Math.abs(value).toLocaleString("en-US", {
    style: "currency",
    currency: "USD",
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  });

  if (isDiff) {
    return value >= 0 ? `+${formattedValue}` : `-${formattedValue}`;
  }

  return formattedValue;
};
