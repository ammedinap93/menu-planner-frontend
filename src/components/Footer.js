import React from "react";
import "../styles/Footer.css";

function Footer() {
  const currentYear = new Date().getFullYear();

  return (
    <footer className="footer">
      <p>Menu Planner &copy; {currentYear}</p>
    </footer>
  );
}

export default Footer;
