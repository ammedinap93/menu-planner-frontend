import React, { useState } from "react";
import axiosInstance from "../axiosConfig";
import { useNavigate, Link } from "react-router-dom";
import "../styles/Form.css";

function LoginForm() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!username || !password) {
      alert("Please enter both username and password");
      return;
    }

    try {
      const response = await axiosInstance.post("/login", {
        username,
        password,
      });
      const { token } = response.data;
      localStorage.setItem("token", token);
      localStorage.setItem("username", username);
      navigate("/");
    } catch (error) {
      console.error("Error during login:", error);
      alert("Invalid username or password");
    }
  };

  return (
    <div className="main-container">
      <h2 className="welcome-text">Welcome to your Monthly Budget</h2>
      <div className="form-container">
        <form className="form" onSubmit={handleSubmit}>
          <input
            type="text"
            placeholder="Username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
          <input
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <button type="submit">Login</button>
          <p>
            Don't have an account? <Link to="/register">Register</Link>
          </p>
        </form>
      </div>
    </div>
  );
}

export default LoginForm;
