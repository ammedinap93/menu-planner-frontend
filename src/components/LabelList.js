import React, { useState, useEffect } from "react";
import axiosInstance from "../axiosConfig";
import LabelForm from "./LabelForm";
import DeleteConfirmationDialog from "./DeleteConfirmationDialog";
import "../styles/List.css";

function LabelList() {
  const [labels, setLabels] = useState([]);
  const [showCreateForm, setShowCreateForm] = useState(false);
  const [showEditForm, setShowEditForm] = useState(false);
  const [selectedLabel, setSelectedLabel] = useState(null);
  const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false);

  useEffect(() => {
    fetchLabels();
  }, []);

  const fetchLabels = async () => {
    try {
      const response = await axiosInstance.get("/labels", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      const labels = response.data;
      setLabels(labels);
    } catch (error) {
      console.error("Error fetching labels:", error);
    }
  };

  const handleCreateLabel = () => {
    setShowCreateForm(true);
  };

  const handleEditLabel = (label) => {
    setSelectedLabel(label);
    setShowEditForm(true);
  };

  const handleDeleteLabel = (label) => {
    setSelectedLabel(label);
    setShowDeleteConfirmation(true);
  };

  const confirmDeleteLabel = async () => {
    try {
      await axiosInstance.delete(`/labels/${selectedLabel.id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      setShowDeleteConfirmation(false);
      fetchLabels();
    } catch (error) {
      console.error("Error deleting label:", error);
    }
  };

  return (
    <div className="list">      
      <div className="column-lists">
        <div className="label-list">
          <h2>Labels</h2>
          <ul>
            {labels.map((label) => (
              <li key={label.id}>
                {label.text}
                {label.user_id && (
                  <div className="list-item-actions">
                    <button
                      className="edit"
                      onClick={() => handleEditLabel(label)}
                    >
                      Edit
                    </button>
                    <button
                      className="delete"
                      onClick={() => handleDeleteLabel(label)}
                    >
                      Delete
                    </button>
                  </div>
                )}
              </li>
            ))}
          </ul>
        </div>        
      </div>
      <div className="list-actions">
        <button className="primary" onClick={handleCreateLabel}>
          Create Custom Label
        </button>
      </div>

      {showCreateForm && (
        <LabelForm
          onClose={() => setShowCreateForm(false)}
          onSubmit={() => {
            setShowCreateForm(false);
            fetchLabels();
          }}
        />
      )}

      {showEditForm && (
        <LabelForm
          label={selectedLabel}
          onClose={() => setShowEditForm(false)}
          onSubmit={() => {
            setShowEditForm(false);
            fetchLabels();
          }}
        />
      )}

      {showDeleteConfirmation && (
        <DeleteConfirmationDialog
          onCancel={() => setShowDeleteConfirmation(false)}
          onConfirm={confirmDeleteLabel}
        />
      )}
    </div>
  );
}

export default LabelList;
