import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import Dropdown from "./Dropdown";
import "../styles/Header.css";

function Header() {
  const [username, setUsername] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    const storedUsername = localStorage.getItem("username");
    if (storedUsername) {
      setUsername(storedUsername);
    } else {
      navigate("/login");
    }
  }, [navigate]);

  const handleLogout = () => {
    localStorage.removeItem("username");
    localStorage.removeItem("token");
    setUsername(null);
    navigate("/login");
  };

  return (
    <header className="header">
      <div className="header-logo">
        <Link to="/">Menu Planner</Link>
      </div>
      <nav className="header-nav">
        <ul>          
          <li>
            <Link to="/labels">Labels</Link>
          </li>
        </ul>
      </nav>
      <div className="header-user">
        {username && (
          <Dropdown
            trigger={<span>Welcome, {username}</span>}
            options={[{ label: "Logout", onClick: handleLogout }]}
          />
        )}
      </div>
    </header>
  );
}

export default Header;
