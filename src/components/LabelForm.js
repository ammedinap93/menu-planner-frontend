import React, { useState, useEffect } from "react";
import axiosInstance from "../axiosConfig";
import "../styles/Form.css";

function LabelForm({ label, onClose, onSubmit }) {
  const [text, setText] = useState("");
  const [color, setColor] = useState("");

  useEffect(() => {
    if (label) {
      setText(label.text);
      setColor(label.color);
    }
  }, [label]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      if (label) {
        await axiosInstance.put(
          `/labels/${label.id}`,
          { text, color },
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          }
        );
      } else {
        await axiosInstance.post(
          "/labels",
          { text, color },
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          }
        );
      }
      onSubmit();
    } catch (error) {
      console.error("Error saving label:", error);
    }
  };

  return (
    <div className="form-overlay">
      <form className="form" onSubmit={handleSubmit}>
        <h3>{label ? "Edit Label" : "Create Label"}</h3>
        <input
          type="text"
          placeholder="Label Text"
          value={text}
          onChange={(e) => setText(e.target.value)}
        />
        <input
          type="text"
          placeholder="Label Color"
          value={color}
          onChange={(e) => setColor(e.target.value)}
        />
        <div className="form-actions">
          <button type="submit">{label ? "Update" : "Create"}</button>
          <button type="button" onClick={onClose}>
            Cancel
          </button>
        </div>
      </form>
    </div>
  );
}

export default LabelForm;
