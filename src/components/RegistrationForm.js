import React, { useState } from "react";
import axiosInstance from "../axiosConfig";
import { useNavigate, Link } from "react-router-dom";
import "../styles/Form.css";

function RegistrationForm() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!username || !password || !confirmPassword) {
      alert("Please fill in all fields");
      return;
    }

    if (password !== confirmPassword) {
      alert("Passwords do not match");
      return;
    }

    if (password.length < 8 || password.length > 32) {
      alert("Password must be between 8 and 32 characters");
      return;
    }

    if (!/\d/.test(password) || !/[a-zA-Z]/.test(password)) {
      alert("Password must contain both letters and numbers");
      return;
    }

    try {
      await axiosInstance.post("/register", {
        username,
        password,
      });
      localStorage.setItem("username", username);
      navigate("/login");
    } catch (error) {
      console.error("Error during registration:", error);
      alert("Registration failed. Please try again.");
    }
  };

  return (
    <div className="main-container">
      <div className="form-container">
        <form className="form" onSubmit={handleSubmit}>
          <input
            type="text"
            placeholder="Username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
          <input
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <input
            type="password"
            placeholder="Confirm Password"
            value={confirmPassword}
            onChange={(e) => setConfirmPassword(e.target.value)}
          />
          <button type="submit">Register</button>
          <p>
            Already have an account? <Link to="/login">Login</Link>
          </p>
        </form>
      </div>
    </div>
  );
}

export default RegistrationForm;
