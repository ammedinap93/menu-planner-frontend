import React from "react";
import "../styles/Dialog.css";

function DeleteConfirmationDialog({ onCancel, onConfirm }) {
  return (
    <div className="dialog-overlay">
      <div className="dialog-content">
        <h3>Confirm Delete</h3>
        <p>Are you sure you want to delete this item?</p>
        <div className="dialog-actions">
          <button onClick={onConfirm}>Delete</button>
          <button onClick={onCancel}>Cancel</button>
        </div>
      </div>
    </div>
  );
}

export default DeleteConfirmationDialog;