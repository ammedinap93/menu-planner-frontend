import * as React from "react";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Layout from "./components/Layout";
import LoginForm from "./components/LoginForm";
import RegistrationForm from "./components/RegistrationForm";
import LabelList from "./components/LabelList";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        path: "labels",
        element: <LabelList />,
      },
    ],
  },
  {
    path: "login",
    element: <LoginForm />,
  },
  {
    path: "register",
    element: <RegistrationForm />,
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
